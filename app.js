const express = require('express');
const bodyParser = require('body-parser');

var app = express();
app.use(bodyParser.json());

app.use(function (req, res, next) {
    console.log('Time:', Date.now())
    next()
  });

app.post('/', function(req, res, next){
    let parameter = req.body;
    console.log("req.body.parameter ", req.body.parameter);
    res.json(parameter);
});

var port = 3000;
app.listen(port);
console.log('Running on port ' + port);